package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface IProjectTaskService {

    Project removeProjectById(final String id);

    Project removeProjectByIndex(final int index);

    Project removeProjectByName(final String name);

    void clearProjects();

}
