package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task findById(final String id);

    Task findByName(final String name);

    Task findByIndex(final int index);

    Task add(final String name, final String description);

    Task removeById(final String id);

    Task removeByIndex(final int index);

    Task removeByName(final String name);

    Task updateByIndex(final int index, final String name, final String description);

    Task updateById(final String id, final String name, final String description);

    void clear();

    int getSize();

    boolean isEmpty();

    Task updateStatus(final Task task, final Status status);

    Task startById(final String id);

    Task startByIndex(final int index);

    Task startByName(final String name);

    Task completeById(final String id);

    Task completeByIndex(final int index);

    Task completeByName(final String name);

    Task updateProjectId(final Task task, final String projectId);

    Task removeFromProject(final Task task, final String projectId);

    List<Task> findAllByProjectId(final String projectId);

}
