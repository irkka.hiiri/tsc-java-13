package ru.tsc.ichaplygina.taskmanager.util;

import java.util.List;

public class ValidationUtil {

    private ValidationUtil() {
    }

    public static boolean isEmptyString(final String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isInvalidListIndex(final int index, final int size) {
        return (index < 0 || index >= size);
    }

    public static boolean isNotFoundInList(final List list, final Object object) {
        return !list.contains(object);
    }

}
