package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    private Project removeProjectWithTasks(final Project project) {
        if (project == null) return null;
        taskRepository.removeAllByProjectId(project.getId());
        projectRepository.remove(project);
        return project;
    }

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Project removeProjectById(final String id) {
        if (isEmptyString(id)) return null;
        final Project project = projectRepository.findById(id);
        return removeProjectWithTasks(project);
    }

    @Override
    public Project removeProjectByIndex(final int index) {
        if (isInvalidListIndex(index, projectRepository.getSize())) return null;
        final Project project = projectRepository.findByIndex(index);
        return removeProjectWithTasks(project);
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (isEmptyString(name)) return null;
        final Project project = projectRepository.findByName(name);
        return removeProjectWithTasks(project);
    }

    @Override
    public void clearProjects() {
        for (Project project : projectRepository.findAll())
            taskRepository.removeAllByProjectId(project.getId());
        projectRepository.clear();
    }

}
