package ru.tsc.ichaplygina.taskmanager.model;

public class Task extends AbstractBusinessEntity {

    private String projectId;

    public Task() {
    }

    public Task(final String name) {
        super(name);
    }

    public Task(final String name, final String description) {
        super(name, description);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }
}
