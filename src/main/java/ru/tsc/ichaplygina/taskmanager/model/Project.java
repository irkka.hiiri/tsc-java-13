package ru.tsc.ichaplygina.taskmanager.model;

public class Project extends AbstractBusinessEntity {

    public Project() {
    }

    public Project(final String name) {
        super(name);
    }

    public Project(final String name, final String description) {
        super(name, description);
    }

}
