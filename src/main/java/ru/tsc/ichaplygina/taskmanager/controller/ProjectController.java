package ru.tsc.ichaplygina.taskmanager.controller;

import ru.tsc.ichaplygina.taskmanager.api.IProjectController;
import ru.tsc.ichaplygina.taskmanager.api.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;
    private final IProjectTaskService projectTaskService;

    public ProjectController(IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    private void showNotFound() {
        printLinesWithEmptyLine(PROJECT_NOT_FOUND);
    }

    private void showRemoveResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(PROJECT_REMOVED);
        else showNotFound();
    }

    private void showUpdateResult(final boolean isSuccess) {
        if (isSuccess) printLinesWithEmptyLine(PROJECT_UPDATED);
        else printLinesWithEmptyLine(PROJECT_UPDATE_ERROR);
    }

    @Override
    public void showList() {
        if (projectService.isEmpty()) {
            printLinesWithEmptyLine(NO_PROJECTS_FOUND);
            return;
        }
        printListWithIndexes(projectService.findAll());
    }

    @Override
    public void clear() {
        final int count = projectService.getSize();
        projectTaskService.clearProjects();
        printLinesWithEmptyLine(count + PROJECTS_CLEARED);
    }

    @Override
    public void create() {
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        if (projectService.add(name, description) == null) printLinesWithEmptyLine(PROJECT_CREATE_ERROR);
        else printLinesWithEmptyLine(PROJECT_CREATED);
    }

    public void showProject(final Project project) {
        if (project == null) showNotFound();
        else printLinesWithEmptyLine(project);
    }

    @Override
    public void showById() {
        final String id = readLine(ID_INPUT);
        final Project project = projectService.findById(id);
        showProject(project);
    }

    @Override
    public void showByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final Project project = projectService.findByIndex(index - 1);
        showProject(project);
    }

    @Override
    public void showByName() {
        final String name = readLine(NAME_INPUT);
        final Project project = projectService.findByName(name);
        showProject(project);
    }

    @Override
    public void updateById() {
        final String id = readLine(ID_INPUT);
        if (projectService.findById(id) == null) {
            showNotFound();
            return;
        }
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = projectService.updateById(id, name, description) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void updateByIndex() {
        final int index = readNumber(INDEX_INPUT);
        if (projectService.findByIndex(index - 1) == null) {
            showNotFound();
            return;
        }
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        final boolean isSuccess = projectService.updateByIndex(index - 1, name, description) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void removeById() {
        final String id = readLine(ID_INPUT);
        final boolean isSuccess = projectTaskService.removeProjectById(id) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByName() {
        final String name = readLine(NAME_INPUT);
        final boolean isSuccess = projectTaskService.removeProjectByName(name) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void removeByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final boolean isSuccess = projectTaskService.removeProjectByIndex(index - 1) != null;
        showRemoveResult(isSuccess);
    }

    @Override
    public void startByName() {
        final String name = readLine(NAME_INPUT);
        final boolean isSuccess = projectService.startByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startById() {
        final String id = readLine(ID_INPUT);
        final boolean isSuccess = projectService.startById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void startByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final boolean isSuccess = projectService.startByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByName() {
        final String name = readLine(NAME_INPUT);
        final boolean isSuccess = projectService.completeByName(name) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeById() {
        final String id = readLine(ID_INPUT);
        final boolean isSuccess = projectService.completeById(id) != null;
        showUpdateResult(isSuccess);
    }

    @Override
    public void completeByIndex() {
        final int index = readNumber(INDEX_INPUT);
        final boolean isSuccess = projectService.completeByIndex(index - 1) != null;
        showUpdateResult(isSuccess);
    }

}
