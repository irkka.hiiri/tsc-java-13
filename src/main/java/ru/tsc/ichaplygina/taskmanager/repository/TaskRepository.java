package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void update(final Task task, final String name, final String description) {
        task.setName(name);
        task.setDescription(name);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task findById(final String id) {
        for (Task task : list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        return list.remove(index);
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public void updateStatus(Task task, Status status) {
        task.setStatus(status);
    }

    @Override
    public Task updateProjectId(Task task, String projectId) {
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        List<Task> list = new ArrayList<>();
        for (Task task : this.list) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> removeAllByProjectId(final String projectId) {
        list.removeIf(task -> projectId.equals(task.getProjectId()));
        return list;
    }
}
